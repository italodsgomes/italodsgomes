<?php
//mostra detalhes do ambiente
//phpinfo();

//echo "<pre>";
//var_dump($_SERVER);

//echo "Seu IP é: ".$_SERVER['REMOTE_ADDR'];

//Vetor co mais de uma dimensao
$alunos[0]['nome'] = 'Italo Gomes 01';
$alunos[0]['bitbucket'] = 'https://bitbucket.org/italodsgomes';

$alunos[1]['nome'] = 'Italo Gomes 02';
$alunos[1]['bitbucket'] = 'https://bitbucket.org/italodsgomes';

$alunos[2]['nome'] = 'Italo Gomes 03';
$alunos[2]['bitbucket'] = 'https://bitbucket.org/italodsgomes';

$alunos[3]['nome'] = 'Italo Gomes 04';
$alunos[3]['bitbucket'] = 'https://bitbucket.org/italodsgomes';

$alunos[4]['nome'] = 'Italo Gomes 05';
$alunos[4]['bitbucket'] = 'https://bitbucket.org/italodsgomes';

//Outra forma

$alunos = array (0 => array ('nome' => 'Italo Gomes 06',
                            'bitbucket' => 'https://bitbucket.org/italodsgomes'),

                            1 => array ('nome' => 'Italo Gomes 07',
                            'bitbucket' => 'https://bitbucket.org/italodsgomes'),
                            
                            2 => array ('nome' => 'Italo Gomes 08',
                            'bitbucket' => 'https://bitbucket.org/italodsgomes'),

                            3 => array ('nome' => 'Italo Gomes 09',
                            'bitbucket' => 'https://bitbucket.org/italodsgomes'),

                            4 => array ('nome' => 'Italo Gomes 10',
                            'bitbucket' => 'https://bitbucket.org/italodsgomes'),

                            5 => array ('nome' => 'Italo Gomes 11',
                            'bitbucket' => 'https://bitbucket.org/italodsgomes'),
);

echo "<pre>";
var_dump($alunos);
