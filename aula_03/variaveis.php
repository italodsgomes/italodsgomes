<?php

echo "<pre>";
// Constante no PHP
define('QTD_PAGINAS', 10);

echo "Valor da minha constante é: " . QTD_PAGINAS;

// Variável para passar valor para uma constante
$ip_do_banco = "192.168.45.12";

define('IP_DO_BANCO', $ip_do_banco);
echo "\nO ip do SGDB é: " . IP_DO_BANCO;

// Constantes mágicas
echo "\nEstou na linha " . __LINE__;
echo "\nEstou na linha " . __LINE__;
echo "\nEste é o arquivo " . __FILE__;


// Muito bom para depurar o código
echo "\n\n";
var_dump ($ip_do_banco);


/*Vetores
*Array
*/
$dias_da_semana = [ 'dom',
                    'seg',
                    'ter',
                    'qua',
                    'qui',
                    'sex',
                    'sab'];

unset($dias_da_semana); // destruir a variável


$dias_da_semana[0] = 'dom';
$dias_da_semana[1] = 'seg';
$dias_da_semana[2] = 'ter';
$dias_da_semana[3] = 'qua';
$dias_da_semana[4] = 'qui';
$dias_da_semana[5] = 'sex';
$dias_da_semana[6] = 'sab';

unset($dias_da_semana);

$dias_da_semana = array(0 => 'dom',
                        1 => 'seg',
                        2 => 'ter',
                        3 => 'qua',
                        4 => 'qui',
                        5 => 'sex',
                        6 => 'sab');

echo("\n\n");
var_dump($dias_da_semana);









echo "</pre>";
